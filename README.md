# proyecto TFM UNIR 

TFM UNIR 2022 project
Virtual Web Spaces Anomalies/Bug Detector Tool



## Description
This project was develop for the UNIR Master Final Project

The main goal is to dectect Anomalies/Bug in Virtual Web Spaces for the Metaverse:
* Point anomalies:
    * Postion
    * Time

* Events chain stream:
    * Probability Matrix heatmap
    * Markov chain graph


## Installation (used libraries)
 1. pip install docopt
 2. pip install rich
 3. pip install pandas
 4. pip install requests
 5. pip install seaborn
 6. pip install markovclick
 7. pip install graphviz
 8. pip install tqdm
 9. pip install networkx
 10. pip install sklearn

## Usage
The next commands runs the developed software tool using the TFM datasets:

**Virtual Web Space: Spaceship**

>   python .\create_SpaceShip_tfm_evaluation_models.py

**Virtual Web Space: FoodBots**
 
>   python .\create_FoodBots_tfm_evaluation_models.py



