from datetime import datetime
import pandas as pd
import numpy as np
import os
import json

# Class for preparing Opensearch Analytics data into transformed dataframe for Anomalies detection 
class OpenSearchVirtualSpaceAnalyticsTransformer:

    def __init__(self, virtual_space_name) -> None:
        self.virtual_space_name = virtual_space_name

    def load_pattern_table(self) -> bool:
        return self.__load_pattern_table()


    def __load_pattern_table(self) -> bool:
        
        file_name = ".\\config\\" + self.virtual_space_name + '_pattern_code.csv'
        try:                        
            self.pattern_table = pd.read_csv(file_name)
        except:
            print(f'Error reading config patter code file {file_name}')
            return False
       
        return True


    def __calculate_seconds_from_timestamp(self, ts):
        fmt = '%Y-%m-%d %H:%M:%S'
        tstamp1 = datetime.strptime(ts, fmt)
    
        return (tstamp1 - datetime(1970,1,1)).total_seconds()

    def __process_json_data(self, eventsData) -> pd.DataFrame:
        #create column data
        col_ts = []
        col_ts_seconds = []
        col_seconds_previous_event = []
        col_user_id = []
        col_client_id = []
        col_virtual_space_id = []
        col_room_id = []
        col_object_type = []
        col_object_id = []
        col_object_action = []
        col_position_x = []
        col_position_y = []   
        col_browser_name = []
        col_browser_version = []
        col_browser_major = []
        col_cpu_architecture = []
        col_cpu_engine_name = []
        col_cpu_engine_version = []
        col_os_name = []
        col_os_version = []
        col_ua  = []
    
        hits = eventsData['hits']['hits']    

        # Process object in hits key
        for hit in hits:

            source = hit["_source"]

            if (source['user_id']) == '':
                continue
            seconds = self.__calculate_seconds_from_timestamp(source['ts'])
            col_ts.append(source['ts'])
            col_ts_seconds.append(seconds)
            col_seconds_previous_event.append(0)
            col_user_id.append(source['user_id'])
            col_client_id.append(source['client_id'])
            col_virtual_space_id.append(source['virtual_space_id'])
            col_room_id.append(source['room_id'])
            col_object_type.append(source['object_type'])
            col_object_id.append(source['object_id'])
            col_object_action.append(source['object_action'])
            if ("object_position") in source:
                position = str(source['object_position'])
                positions = position.replace("[","").replace("]","").replace("\"","").split(",")
                col_position_x.append(float(positions[0]))
                col_position_y.append(float(positions[2]))            
            else: 
                col_position_x.append(np.nan)
                col_position_y.append(np.nan)            

            device_data = source['device_data']
            jsonObject = json.loads(device_data)
        
            if ("engine") in jsonObject:
                jsonEngine = jsonObject["engine"]
                col_cpu_engine_name.append(jsonEngine["name"])
                col_cpu_engine_version.append(jsonEngine["version"])
            else:
                col_cpu_engine_name.append("NA")
                col_cpu_engine_version.append("NA")

            #extract browser data
            if ("browser") in jsonObject:
                jsonBrowser = jsonObject["browser"]
                col_browser_name.append(jsonBrowser['name'])
                col_browser_version.append(jsonBrowser['version'])
                col_browser_major.append(jsonBrowser['major'])
            else:
                col_browser_name.append("NA")
                col_browser_version.append("NA")
                col_browser_major.append("NA")

            #extract cpu data
            if ("cpu") in jsonObject:
                jsonCpu = jsonObject["cpu"]                 
                if ("architecture") in jsonCpu:
                    col_cpu_architecture.append(jsonCpu['architecture'])
                else: 
                    col_cpu_architecture.append("NA")
            else:
                col_cpu_architecture.append("NA")
            

            #extract os data
            if ("os") in jsonObject:
                jsonOs = jsonObject["os"]       
                col_os_name.append(jsonOs['name'])
                if ("version") in jsonOs:
                    col_os_version.append(jsonOs['version'])
                else: 
                    col_os_version.append("NA")
            else:
                col_os_name.append("NA")
                col_os_version.append("NA")

            #extract ua data
            if ("ua") in jsonObject:
                col_ua.append(jsonObject['ua'])
            else:
                col_ua.append('NA')

        # Create events dictionary from processed hits key
        events = {
            'ts': col_ts,
            'ts_seconds': col_ts_seconds,
            'seconds_previous_event': col_seconds_previous_event,
            'user_id': col_user_id,
            'client_id': col_client_id,
            'virtual_space_id': col_virtual_space_id,
            'room_id': col_room_id,
            'object_type': col_object_type,
            'object_id': col_object_id,
            'object_action': col_object_action,
            'position_x': col_position_x,
            'position_y': col_position_y,     
            'browser_name': col_browser_name,
            'browser_version': col_browser_version,
            'browser_major': col_browser_major,
            'cpu_architecture': col_cpu_architecture,
            'cpu_engine_name': col_cpu_engine_name,
            'cpu_engine_version':col_cpu_engine_version,
            'os_name': col_os_name,
            'os_version': col_os_version,
            'os_ua': col_ua
        }

        # Create Pandas dataframe from events
        df = pd.DataFrame(data = events)

        return df

    def __order_events_and_calculate_seconds_from_previous_event(self, df: pd.DataFrame) -> pd.DataFrame:
        # Order events and calculate seconds from previous event
        df = df.sort_values(['user_id', 'ts'])

        col_seconds_previous_event = []
        df = df.reset_index()  # make sure indexes pair with number of rows
        previous_user_id = ''
        previous_seconds = 0
        for index, row in df.iterrows():    
            if (index > 0):
                if (row['user_id'] == previous_user_id):
                    col_seconds_previous_event.append(row['ts_seconds'] - previous_seconds)
                else:
                    col_seconds_previous_event.append(0)
            else:
                col_seconds_previous_event.append(0)

            previous_seconds = row['ts_seconds']
            previous_user_id = row['user_id']

        # Assign column seconds from previous_events
        df['seconds_previous_event'] = col_seconds_previous_event

        return df


    # Store this sessions for technical analyze
    def __store_sessions_with_only__one_event(self, df: pd.DataFrame, date):
        unique_data = np.invert(df.duplicated(subset=['user_id'], keep=False))
        df = df[unique_data]
        return df
        


    # Delete sessions with only one event
    def __delete_sessions_with_only_one_event(self, df: pd.DataFrame):               
        non_unique_data = df[df.duplicated(subset=['user_id'], keep=False)]
        df = non_unique_data
        return df
    
    # Create dataframe with processed info and events pattern code
    def __create_final_dataframe(self, df: pd.DataFrame) -> pd.DataFrame:
        if len(df)==0:
            return pd.DataFrame()

        df2 = pd.DataFrame()
        df2['ts'] = df['ts']
        df2['seconds_previous_event']  = df['seconds_previous_event'] 
        df2['user_id'] = df['user_id']
        df2['pattern'] = df['virtual_space_id'] + '-' + df['room_id'] + '-' + df['object_id'] + '-' + df['object_action']   
        df2['room_id'] = df['room_id']
        df2['object_id'] = df['object_id']
        df2['object_type'] = df['object_type']
        df2['position_x'] = df['position_x']
        df2['position_y'] = df['position_y']
        df2['browser_name'] = df['browser_name']
        df2['browser_version'] = df['browser_version']
        df2['browser_major'] = df['browser_major']
        df2['browser_name_version'] = df['browser_name'] + '-' + df['browser_version']
        df2['cpu_architecture'] = df['cpu_architecture']
        df2['cpu_engine_name'] = df['cpu_engine_name']
        df2['cpu_engine_version'] = df['cpu_engine_version']
        df2['cpu_engine_name_version'] = df['cpu_engine_name'] + '-' + df['cpu_engine_version']    
        df2['os_name'] = df['os_name']
        df2['os_version'] = df['os_version']
        df2['os_name_version'] = df['os_name'] + '-' + df['os_version']    
        df2['os_ua'] = df['os_ua']

   
        # Insert stored pattern_code previously calculated to be sure always the same pattern is used
        df2 = pd.merge(df2, self.pattern_table, how="inner", on="pattern")

        

        return df2



    def transform(self, eventsData, date):
        try:      
           
            df = self.__process_json_data(eventsData)
            df = self.__order_events_and_calculate_seconds_from_previous_event(df)
            df_sessions_with_one_event = self.__store_sessions_with_only__one_event(df, date)
            df = self.__delete_sessions_with_only_one_event(df)
            df = self.__create_final_dataframe(df)

            return df, df_sessions_with_one_event     
        except BaseException as err:
            print(f"Unexpected {err=}, {type(err)=}")
            return None

   




    
