# Proyecto TFM
 ## Virtual Web Spaces Anomalies/Bug Detector Tool

### Instalación librerías necesarias
 1. pip install docopt
 2. pip install rich
 3. pip install pandas
 4. pip install requests
 5. pip install seaborn
 6. pip install markovclick
 7. pip install graphviz
 8. pip install tqdm
 9. pip install networkx
 10. pip install sklearn