import datetime as dt
import pandas as pd
from AnomalyDetectors.DiscreteEventsSequenceAnomalyDetector import DiscreteEventsSequenceAnomalyDetector
from AnomalyDetectors.PointAnomalyDetector import PointAnomalyDetector
import matplotlib.pyplot as plot
import warnings


#get last processed sessions date
def get_last_processed_date():
    return dt.datetime.now() - dt.timedelta(days=2)

def main():
    warnings.filterwarnings("ignore")

    virtual_space_name = 'Spaceship'

 

    # Creates Virtual Space Spaceship Point Anomaly Detector
    point_anomaly_detector = PointAnomalyDetector(virtual_space_name)


    # Creates Virtual Space Spaceship Sequence Anomaly Detector
    discrete_events_sequence_anomaly_detector = DiscreteEventsSequenceAnomalyDetector(virtual_space_name)

    
    # Write Full dataframe into a csv file
    full_dataframe  = pd.read_csv("./input/tfm_spaceship_evaluation_full_dataframe.csv")
   
    
     # Checks for Point Anomalies
    point_anomaly_detector.create_models(full_dataframe)

    result_df_position, result_df_time = point_anomaly_detector.get_possible_anomalies(full_dataframe)

    str_file = "./output/datasets/" + virtual_space_name + "_position_anomaly_processed.csv" 
    result_df_position.to_csv(str_file, index=False) 
    print("Processed: ", str_file)

    str_file = "./output/datasets/" + virtual_space_name + "_time_anomaly_processed.csv" 
    result_df_time.to_csv(str_file, index=False) 
    print("Processed: ", str_file)


    # Checks for Discrete Events Sequences Anomalies
    discrete_events_sequence_anomaly_detector.create_models(full_dataframe)
       
    result_df = discrete_events_sequence_anomaly_detector.get_possible_anomalies(full_dataframe)

    str_file = "./output/datasets/" + virtual_space_name + "_chain_anomaly_processed.csv" 
    result_df.to_csv(str_file, index=False) 
    print("Processed: ", str_file)




    # Shows plots for No sequence Anomalies
    point_anomaly_detector.plot_decision_boundaries(full_dataframe)



    #Shows the plots for the Discrete events sequences Markovs models created
    discrete_events_sequence_anomaly_detector.show_created_models()

   

if __name__ == '__main__':
    main()