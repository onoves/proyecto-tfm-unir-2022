import pandas as pd
from Models.MarkovChainModel import MarkovChainModel

# Class for detecting anomalies in Discrete Sequence Data
class DiscreteEventsSequenceAnomalyDetector:

    def __init__(self, virtual_space_name) -> None:

        self.markov_chain_room_models = {}
        self.virtual_space_name = virtual_space_name
        pass


    def __get_session_stream_events(self, room_events: pd.DataFrame):
        stream_events = []
        sessions = room_events['user_id'].unique()
        for session in sessions:
            session_events = room_events['user_id'] == session
            stream_codes_df = (room_events[session_events])
            array = stream_codes_df['pattern_code'].to_numpy()            
            sequence = array.tolist()            
            room_events.loc[session_events,'sequence'] = " ".join(sequence)
            stream_events.append(sequence)
        
        return room_events, stream_events
    
    def __create_models(self, room_events: pd.DataFrame, room: str) -> pd.DataFrame:
       

        print(f'  Creating Sequence Chain Stream  anomalies/bug detection model for room: {room}')

        sequence_room_events, stream_events = self.__get_session_stream_events(room_events)

        # Select created Markov model or creates new one for the current room
        if room in self.markov_chain_room_models:
            self.markov_chain_room_models[room].construct_model(stream_events)            
        else:
            self.markov_chain_room_models[room] = MarkovChainModel(virtual_space_name=self.virtual_space_name, room=room)
            self.markov_chain_room_models[room].construct_model(stream_events)



    def create_models(self, df: pd.DataFrame): 
        # Get Virtual Space Rooms - each room will have it's own model
        rooms = df['room_id'].unique()
        room_events = []
        for room in rooms:
            room_events = df[df['room_id'] == room]
            self.__create_models(room_events, room)


    def __process_rom_events(self, room_events: pd.DataFrame, room: str) -> pd.DataFrame:
     
        print(f'  Predicting Sequence Chain Stream anomalies/bug detection for room: {room}')
     
        sequence_room_events, stream_events = self.__get_session_stream_events(room_events) 
        # Get unique sequences and calculates the probability
        sequences = sequence_room_events['sequence'].unique()
        for sequence in sequences:
            sequence_list = sequence.split()
            select = sequence_room_events['sequence'] == sequence   
            sequence_probability = self.markov_chain_room_models[room].get_probability_event_sequence(sequence_list)
            sequence_room_events.loc[select,'probability'] = sequence_probability
        
        return sequence_room_events   

    def get_possible_anomalies(self, df: pd.DataFrame) -> pd.DataFrame:
        
        result_df = pd.DataFrame()
        # Get Virtual Space Rooms - each room will have it's own model
        rooms = df['room_id'].unique()
        room_events = []
        for room in rooms:
            room_events = df[df['room_id'] == room]
            events = self.__process_rom_events(room_events, room)
            result_df = result_df.append(events)

        # Remove column Pattern_code because Session sequence is calculated
        result_df = result_df.drop(['seconds_previous_event','room_id', 'position_x','position_y','object_id', 'object_type', 'pattern','pattern_code'], 1)

        # Return df
        return result_df
        

    def show_created_models(self):
        for element in self.markov_chain_room_models:
            model = self.markov_chain_room_models[element]
            model.plot_markov_probability_matrix_heatmap()
            model.plot_markov_probability_chain_graph()
