import numpy as np
import pandas as pd

# Graphics
# ==============================================================================
import matplotlib.pyplot as plot
from matplotlib.colors import ListedColormap

# Models and proccesing data
# ==============================================================================
from sklearn.ensemble import IsolationForest
from sklearn.neighbors import LocalOutlierFactor
from sklearn import svm
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import  StandardScaler
from sklearn.inspection import DecisionBoundaryDisplay


# Model for Outliers, it will implement iForest, LOF, OneSVC
class TimeAnomalyOutliersModel:

    def __init__(self, virtual_space_name: str, room: str) -> None:
        self.events_data = pd.DataFrame()   
        self.room = room
        self.virtual_space_name = virtual_space_name       
        self.iForest = IsolationForest(n_estimators=1000, max_features=2, random_state=42)
        self.lof = LocalOutlierFactor(n_neighbors=20, algorithm='brute')
        self.one_class_svm = svm.OneClassSVM(nu=0.5, kernel='rbf', gamma='auto')
        self.standard_scaler = StandardScaler() 

        pass

     # Convert events data
    def __convert_events_data(self, events) -> pd.DataFrame:

        df_temp = events.copy()
        # Events per session / user
        df_session_events = df_temp.groupby(['user_id']).size().reset_index(name='count')      
       
        # Time spend per session
        df_session_time = df_temp.groupby(['user_id'])['seconds_previous_event'].median().reset_index()

        # Merge dataframes using key user_id
        df = pd.merge(df_session_events, df_session_time, how="inner", on="user_id")

        #Rename col
        df.rename(columns = {'count':'session_events', 'seconds_previous_event':'median_seconds_previous_event'}, inplace = True)
        df['virtual_space_name'] = self.virtual_space_name
        df['room'] = self.room
        # Drop null values
        df = df.dropna()
       
        return df

    # Constructs models
    def construct_model(self, events):       
        df = self.__convert_events_data(events)        
        self.events_data = pd.concat([self.events_data, df])
        
    
    # Fit the models
    def fit(self):
        if self.events_data.empty == False:
            
            df_proceso = self.events_data[['session_events', 'median_seconds_previous_event']].copy()

            # Normalize data           
            df_escalado = self.standard_scaler.fit_transform(df_proceso)
            df_escalado = pd.DataFrame(df_escalado) 
            df_escalado = df_escalado.rename(columns = {0: "session_events", 1: "median_seconds_previous_event"})
                  

            X,y = df_escalado, range(len(df_escalado.index))
            X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=0.2, random_state = 42)
            
            self.iForest.fit(X_train.values)    
                                   
            self.one_class_svm.fit(X_train.values)


    # Predict
    def predict(self, events)-> pd.DataFrame:
        if self.events_data.empty == False:
            df = self.__convert_events_data(events)    

            df_proceso = df[['session_events', 'median_seconds_previous_event']].copy()

            # Normalize data             
            df_escalado = self.standard_scaler.transform(df_proceso)
            df_escalado = pd.DataFrame(df_escalado) 
            df_escalado = df_escalado.rename(columns = {0: "session_events", 1: "median_seconds_previous_event"})           

            y_iForest_pred_outliers = self.iForest.predict(df_escalado.values)            
            y_lof_pred_outliers = self.lof.fit_predict(df_escalado.values)
            y_one_class_svm_pred_outliers = self.one_class_svm.predict(df_escalado.values)
            
            # Assign prediction to input dataset
            df['iForest_pred_outliers'] = y_iForest_pred_outliers
            df['LOF_pred_outliers'] = y_lof_pred_outliers
            df['OCSVM_pred_outliers'] = y_one_class_svm_pred_outliers
            
            return df
        else:
            return pd.DataFrame()


    # Plot decision boundaries by model
    def __plot_by_model(self, model, df, ax,  title):

        df_plot = df.copy()
        plot.title(title)

        if title == 'LOF':
            # When LOF is used for outlier detection, the estimator has no predict, decision_function and score_samples methods
            y_predict = model.fit_predict(df_plot.values)             
        else:
            DecisionBoundaryDisplay.from_estimator(model, df_plot, cmap=plot.cm.PuBu, ax = ax,  alpha=0.8, eps=0.5)                          
            y_predict = model.predict(df_plot.values) 

        df_plot['outlier'] = y_predict
        # Outlier filter
        is_outlier = df_plot['outlier'] == -1
        is_ok = df_plot['outlier'] == 1
        df_outlier = df_plot[is_outlier]
        df_ok = df_plot[is_ok]
        
        # Scatter Plot Ok data and Possible anomalies detected
        b1 = plot.scatter(df_outlier.iloc[:, 0], df_outlier.iloc[:, 1], c="red", s=20, edgecolor="k")
        b2 = plot.scatter(df_ok.iloc[:, 0], df_ok.iloc[:, 1], c="blue", s=20, edgecolor="k")
            
        # Plot legend and Axis labels
        plot.legend(
            [b1, b2],
            ["outlier", "ok"],
            loc="upper left",
        )
        plot.xlabel('Session events')
        plot.ylabel('Median seconds from previous event')
       
        return df_plot

    

    def __get_is_outlier_by_three_algorithms(self, row):
        if row['iForest'] == 1 or row['LOF'] == 1 or row['OCC-SVM'] == 1:
            return 1
        else:
             return -1

    def __plot(self, title):
        
        df_proceso = self.events_data[['session_events', 'median_seconds_previous_event']].copy()

        # Normalize data
        df_escalado = self.standard_scaler.transform(df_proceso)
        df_escalado = pd.DataFrame(df_escalado) 
        df_escalado = df_escalado.rename(columns = {0: "session_events",1: "median_seconds_previous_event"})
      

        figure = plot.figure(figsize=(10,12), dpi=120, clear=True)             
        figure.suptitle('Possible Time data anomalies detection in: ' + title)
                         

        # Isolation Forest plot
        ax = plot.subplot(2, 2, 1)  
        df_plot_iForest = self.__plot_by_model(model = self.iForest, df=df_escalado, ax = ax, title = 'iForest')

        # Local Outlier Factor plot
        ax = plot.subplot(2, 2, 2)  
        df_plot_lof = self.__plot_by_model(model = self.lof, df=df_escalado, ax = ax, title = 'LOF')

        # One class SVM
        ax = plot.subplot(2, 2, 3)  
        df_plot_one_class_svm = self.__plot_by_model(model = self.one_class_svm, df=df_escalado, ax = ax, title = 'OCC-SVM')


        # Three algorithm 
        df_escalado['iForest']  = df_plot_iForest['outlier']
        df_escalado['LOF']  = df_plot_lof['outlier']
        df_escalado['OCC-SVM']  = df_plot_one_class_svm['outlier']
        df_escalado['outlier'] = df_escalado.apply (lambda row: self.__get_is_outlier_by_three_algorithms(row), axis=1)

        ax = plot.subplot(2, 2, 4)  
        plot.title("Combination iForest - LOF - OCC-SVM")  
        # Outlier filter
        is_outlier = df_escalado['outlier'] == -1
        is_ok = df_escalado['outlier'] == 1
        df_outlier = df_escalado[is_outlier]
        df_ok = df_escalado[is_ok]
        
        # Scatter Plot Ok data and Possible anomalies detected
        b1 = plot.scatter(df_outlier.iloc[:, 0], df_outlier.iloc[:, 1], c="red", s=20, edgecolor="k")
        b2 = plot.scatter(df_ok.iloc[:, 0], df_ok.iloc[:, 1], c="blue", s=20, edgecolor="k")
            
        # Plot legend and Axis labels
        plot.legend(
            [b1, b2],
            ["outlier", "ok"],
            loc="upper left",
        )
        plot.xlabel('Session events')
        plot.ylabel('Median seconds from previous event')




        plot.savefig(".\\output\generated_models\\time_" + title + ".png" , dpi=120)
        
        #plot.tight_layout()
        plot.show()
        plot.close() 

    # Plot Decision Boundaries
    def plot_decision_boundaries(self, events) -> None:      

        if len(self.events_data) > 1:
          
            title = self.virtual_space_name + " " + self.room            
            self.__plot(title=title)            
           