"""Virtual Web Spaces Anomalies/Bug Detector Tool
Usage:
  anomaly_detector.py  virtual_space <space_name> start_date <start_date> end_date <end_date> [--show_point_models] [--show_seq_models]
  anomaly_detector.py (-h | --help)
  anomaly_detector.py --extended-help
  anomaly_detector.py --version

Options:
  -h --help           Show this screen.
  -v --version        Show version.
  --extended-help
  --show_point_models  Show point anomalies models
  --show_seq_models    Show sequence anomalies models 

Examples:
  anomaly_detector.py virtual_space FoodBots start_date 2022-04-18 end_date 2022-04-20 --show_point_models --show-seq_models

"""
from docopt import docopt
from Markdown.markdown_help import MarkdownHelp
from AnomalyDetectorCore import AnomalyDetectorCore
from datetime import datetime


def validate_date(date_text):
  try:
    datetime.strptime(date_text, '%Y-%m-%d')            
  except ValueError:
    return False
  
  return True


def validate_data_for_anomalies_deteccion_process(arguments):
  if arguments['virtual_space'] and arguments['start_date'] and arguments['end_date']:
   
    start_date = arguments['<start_date>']
    end_date = arguments['<end_date>']
    space_name = arguments['<space_name>']

    virtual_spaces_allowed = ['FoodBots', 'Spaceship']

    if space_name not in virtual_spaces_allowed:
      print(f'At the moment only this Virtual Web Spaces are allowed:  {",".join(virtual_spaces_allowed)}')
      return



    if validate_date(start_date) == False:
      print(f'Start input date ({start_date}) incorrect date format, should be YYYY-MM-DD"')      
      return

    if validate_date(end_date) == False:
      print(f'End input date ({end_date}) incorrect date format, should be YYYY-MM-DD"')      
      return
    
    start_date = datetime.strptime(start_date, '%Y-%m-%d')
    end_date = datetime.strptime(end_date, '%Y-%m-%d')
    days = (end_date - start_date).days
  

    if days < 0:
      print('End date should be greater than start_date')
      return 

    anomaly_detector_core = AnomalyDetectorCore(space_name= space_name,
                                                  start_date= start_date,
                                                  end_date= end_date,
                                                  show_point_models= arguments['--show_point_models'],
                                                  show_sequence_models= arguments['--show_seq_models'])
    anomaly_detector_core.process()
                                                  
    

def start_anomalies_deteccion_process():
  print('Starting Anomalies/Bug Detection in Virtual Web Space')


def process_input_arguments(arguments):
    
    if arguments['--extended-help'] == True:
      markdown_help = MarkdownHelp()
      markdown_help.print_help()
      return

   
    if (validate_data_for_anomalies_deteccion_process(arguments)):
      start_anomalies_deteccion_process()


if __name__ == '__main__':
    arguments = docopt(__doc__, help=True, version='Virtual Web Spaces Anomalies/Bug Detector Tool 1.0')
    
    process_input_arguments(arguments)



    